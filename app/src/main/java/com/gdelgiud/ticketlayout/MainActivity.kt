package com.gdelgiud.ticketlayout

import android.os.Bundle
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), OnSeekBarChangeListener {

    private lateinit var ticketLayout: TicketLayout
    private lateinit var cornerRadiusSeekbar: SeekBar
    private lateinit var holeRadiusSeekbar: SeekBar
    private lateinit var cornerRadiusLabel: TextView
    private lateinit var holeRadiusLabel: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ticketLayout = ticket_layout
        ticketLayout.holeRadius = resources.getDimension(R.dimen.default_hole_radius)
        ticketLayout.cornerRadius = resources.getDimension(R.dimen.default_corner_radius)
        ticketLayout.backgroundRes = R.drawable.ice_cream
        cornerRadiusLabel = corner_radius_label
        holeRadiusLabel = hole_radius_label
        configureCornerRadiusSeekBar()
        configureHoleRadiusSeekBar()
    }

    private fun configureHoleRadiusSeekBar() {
        holeRadiusSeekbar = hole_radius_seekbar
        holeRadiusSeekbar.setOnSeekBarChangeListener(this)
        holeRadiusSeekbar.progress = pxToDp(ticketLayout.holeRadius).toInt()
    }

    private fun configureCornerRadiusSeekBar() {
        cornerRadiusSeekbar = corner_radius_seekbar
        cornerRadiusSeekbar.setOnSeekBarChangeListener(this)
        cornerRadiusSeekbar.progress = pxToDp(ticketLayout.cornerRadius).toInt()
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        when (seekBar) {
            cornerRadiusSeekbar -> onCornerRadiusSeekbarChanged(progress)
            holeRadiusSeekbar -> onHoleRadiusSeekbarChanged(progress)
        }
    }

    private fun onHoleRadiusSeekbarChanged(progress: Int) {
        holeRadiusLabel.text = getString(R.string.hole_radius, progress)
        ticketLayout.holeRadius = dpToPx(progress.toFloat())
    }

    private fun onCornerRadiusSeekbarChanged(progress: Int) {
        cornerRadiusLabel.text = getString(R.string.corner_radius, progress)
        ticketLayout.cornerRadius = dpToPx(progress.toFloat())
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        // no-op
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        // no-op
    }

    fun dpToPx(dp: Float): Float {
        return dp * resources.displayMetrics.density
    }

    fun pxToDp(px: Float): Float {
        return px / resources.displayMetrics.density
    }
}
