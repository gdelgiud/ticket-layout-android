package com.gdelgiud.ticketlayout

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.annotation.DrawableRes
import androidx.annotation.Px


class TicketLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    @Px
    var cornerRadius: Float = 50f
        set(value) {
            field = value
            requestLayout()
        }

    @Px
    var holeRadius: Float = 25f
        set(value) {
            field = value
            requestLayout()
        }

    @DrawableRes
    var backgroundRes: Int? = null
        set(value) {
            if (value == null) {
                clearBackground()
            } else {
                setBackground(value)
            }
            field = value
        }

    private fun clearBackground() {
        image?.recycle()
    }

    private fun setBackground(value: Int) {
        image?.recycle()
        image = BitmapFactory.decodeResource(resources, value)
    }

    private val paint: Paint = Paint()

    private val path: Path = Path()
    private var image: Bitmap? = null
    private val srcRect: Rect = Rect()

    private val dstRect: Rect = Rect()

    init {
        setWillNotDraw(false)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        onShapeChanged()
    }

    private fun onShapeChanged() {
        createShape()
        image?.let {
            calculateBitmapRects(it)
        }
    }

    private fun calculateBitmapRects(image: Bitmap) {
        setBitmapClip(image)
        dstRect.set(0, 0, width - 1, height - 1)
    }

    private fun setBitmapClip(image: Bitmap) {
        val aspectRatio = width.toFloat() / height.toFloat()

        if (aspectRatio > 1) {
            setHorizontalLayoutClip(image, aspectRatio)
        } else {
            setVerticalLayoutClip(image, aspectRatio)
        }
    }

    private fun setVerticalLayoutClip(image: Bitmap, aspectRatio: Float) {
        val clipWidth = image.height * aspectRatio
        val side = ((image.width - clipWidth) / 2).toInt()
        srcRect.set(side, 0, image.width - side, image.height - 1)
    }

    private fun setHorizontalLayoutClip(image: Bitmap, aspectRatio: Float) {
        val clipHeight = image.width / aspectRatio
        val side = ((image.height - clipHeight) / 2).toInt()
        srcRect.set(0, side, image.width - 1, image.height - side)
    }

    private fun createShape() {
        path.reset()
        val widthF = width.toFloat()
        val heightF = height.toFloat()
        val cornerDiameter = cornerRadius * 2
        val holeDiameter = holeRadius * 2
        val middleHeight = (heightF - cornerDiameter - holeDiameter) / 2f
        val holeTop = cornerRadius + middleHeight
        val holeBottom = holeTop + holeDiameter
        path.addArc(RectF(0f, 0f, cornerDiameter, cornerDiameter), 180f, 90f)
        path.lineTo(widthF - cornerDiameter, 0f)
        path.arcTo(RectF(widthF - cornerDiameter, 0f, widthF, cornerDiameter), 270f, 90f)
        path.lineTo(widthF, holeTop)
        path.arcTo(RectF(widthF - holeRadius, holeTop, widthF + holeRadius, holeBottom), 270f, -180f)
        path.lineTo(widthF, heightF - cornerDiameter)
        path.arcTo(RectF(widthF - cornerDiameter, heightF - cornerDiameter, widthF, heightF), 0f, 90f)
        path.lineTo(cornerDiameter, heightF)
        path.arcTo(RectF(0f, heightF - cornerDiameter, cornerDiameter, heightF), 90f, 90f)
        path.lineTo(0f, holeBottom)
        path.arcTo(RectF(-holeRadius, holeTop, holeRadius, holeBottom), 90f, -180f)
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {c ->
            c.save()
            if (image == null) {
                c.drawPath(path, paint)
            } else {
                c.clipPath(path)
                c.drawBitmap(image!!, srcRect, dstRect, paint)
            }
            c.restore()
        }
        super.onDraw(canvas)
    }

    override fun onDrawForeground(canvas: Canvas?) {
        canvas?.clipPath(path)
        super.onDrawForeground(canvas)
    }
}